<?php
 
namespace app\controllers;
use Yii;
use yii\data\SqlDataProvider;
 
class RelatoriosController extends \yii\web\Controller
{
   public function actionIndex()
   {
       return $this->render('index');
   }
   public function actionRelatorio1()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT AWPESSOAS.NOME, COUNT(AWRECARGA.ID) AS QUANTIDADE
        FROM AWPESSOAS JOIN AWRECARGA ON AWPESOAS.IDCODICO = AWRECARGA.PESSOAS
        GROUP BY AWPESSOAS.IDCODICO
        ORDER BY COUNT(AWRECARGA.ID) DESC',
            ]
        );
        
        return $this->render('relatorio1', ['resultado' => $consulta]);
   }
 

}

