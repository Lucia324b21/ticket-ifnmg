create database trabalhofinal;
use trabalhofinal;
show tables;
create table AwPESSOAS(
IDCODICO int auto_increment,
NOME varchar(30),
   PRIMARY KEY (IDCODICO)
);

create table AwRECARGA(
DATA  date,
VALOR numeric(7,2),
PESSOAS int,
 ID INT AUTO_INCREMENT,
   PRIMARY KEY (ID),
foreign key(PESSOAS) references AwPESSOAS(IDCODICO)
);

create table AwENTRADA(
DATA  date,
VALOR numeric(9,2),
PESSOAS int,
 ID INT AUTO_INCREMENT,
   PRIMARY KEY (ID),
foreign key(PESSOAS) references AwPESSOAS(idcodico)
);