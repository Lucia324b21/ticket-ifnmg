<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AwEntrada */

$this->title = 'Criar Entrada';
$this->params['breadcrumbs'][] = ['label' => 'Aw Entradas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aw-entrada-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
