<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AwEntradaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aw-entrada-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'DATA') ?>

    <?= $form->field($model, 'VALOR') ?>

    <?= $form->field($model, 'PESSOAS') ?>

    <?= $form->field($model, 'ID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
