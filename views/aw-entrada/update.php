<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AwEntrada */

$this->title = 'Update Aw Entrada: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Aw Entradas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="aw-entrada-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
