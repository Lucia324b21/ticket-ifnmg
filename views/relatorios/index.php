<?php
 
use yii\helpers\Html;
 
$this->title = 'Relatórios';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div class="relatorios-index">
 
   <h1><?= Html::encode($this->title) ?></h1>
   <?= Html::a('Quantidade de recargas por pessoa', ['relatorio1'], ['class' => 'btn btn-success']) ?>

 
</div>

