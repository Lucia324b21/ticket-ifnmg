<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\AwPessoas;

/* @var $this yii\web\View */
/* @var $model app\models\awrecarga */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="awrecarga-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'DATA')->textInput(['type'=>'date']) ?>

    <?= $form->field($model, 'VALOR')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PESSOAS')->
       dropDownList(ArrayHelper::map(AwPessoas::find()
           ->orderBy('NOME')
           ->all(),'IDCODICO','NOME'),
           ['prompt' => 'Selecione um Pessoa'] )
?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
