<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\awrecarga */

$this->title = 'Criar Recarga';
$this->params['breadcrumbs'][] = ['label' => 'Awrecargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="awrecarga-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
