<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\awrecarga */

$this->title = 'Update Awrecarga: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Awrecargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="awrecarga-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
