<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\awrecergaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recargas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="awrecarga-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Criar nova recarga', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'DATA',
            'VALOR',
            ['attribute'=>'pESSOAS.NOME','label'=>'pessoas'],
            'ID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
