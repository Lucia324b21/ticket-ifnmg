<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AwPessoas */

$this->title = 'Criar Pessoas';
$this->params['breadcrumbs'][] = ['label' => 'Aw Pessoas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aw-pessoas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
