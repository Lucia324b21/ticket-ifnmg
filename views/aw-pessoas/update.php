<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AwPessoas */

$this->title = 'Update Aw Pessoas: ' . $model->IDCODICO;
$this->params['breadcrumbs'][] = ['label' => 'Aw Pessoas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IDCODICO, 'url' => ['view', 'id' => $model->IDCODICO]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="aw-pessoas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
