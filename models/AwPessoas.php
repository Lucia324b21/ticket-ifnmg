<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "AwPESSOAS".
 *
 * @property int $IDCODICO
 * @property string $NOME
 */
class AwPESSOAS extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'AwPESSOAS';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NOME'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDCODICO' => 'Idcodico',
            'NOME' => 'Nome',
        ];
    }
}
