<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "awrecarga".
 *
 * @property string $DATA
 * @property string $VALOR
 * @property int $PESSOAS
 * @property int $ID
 *
 * @property Awpessoas $pESSOAS
 */
class AwRecarga extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'awrecarga';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DATA'], 'safe'],
            [['VALOR'], 'number'],
            [['PESSOAS'], 'integer'],
            [['PESSOAS'], 'exist', 'skipOnError' => true, 'targetClass' => Awpessoas::className(), 'targetAttribute' => ['PESSOAS' => 'IDCODICO']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DATA' => 'Data',
            'VALOR' => 'Valor',
            'PESSOAS' => 'Pessoas',
            'ID' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPESSOAS()
    {
        return $this->hasOne(Awpessoas::className(), ['IDCODICO' => 'PESSOAS']);
    }
}
